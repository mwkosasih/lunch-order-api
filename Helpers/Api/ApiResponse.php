<?php namespace Helpers\Api;

/**
 * Class For API Response, Objective Reason.
 */
class ApiResponse
{

    const SUCCESS = 200;
    const SUCCESS_NO_CONTENT = 204;
    const ERR_INTERNAL_ERROR = 500;
    const ERR_INVALID_LOGIN = 403;
    const ERR_INVALID_ARGUMENT = 400;
    const ERR_INVALID_PARAMETER = 4;
    const ERR_UNAUTHORIZED = 401;
    const ERR_INACTIVE_ACCOUNT = 6;
    const ERR_INVALID_SESSION = 7;
    const ERR_INVALID_PERMISSION = 8;
    const ERR_INVALID_ACCESS = 9;
    const ERR_DUPLICATE_LOGIN = 10;
    const ERR_INVALID_USER = 11;
    const ERR_INVALID_DATE = 12;
    const ERR_INVALID_OUTLET = 13;
    const ERR_INVALID_AREA = 14;
    const ERR_INACTIVE_SUBDOMAIN = 15;
    const ERR_PROCESS_SUBDOMAIN = 16;
    const ERR_SUSPENDED_SUBDOMAIN = 17;
    const ERR_INVALID_TRANSACTION = 430;
    const ERR_DUPLICATE_TRANSACTION = 431;
    const DATA_IS_EMPTY = 23;
    const ERR_USER_DELETED = 24;
    const ERR_USER_DELETE_IN_OUTLET = 25;
    const ERR_USER_DELETE_VALIDATION = 26;
    const ERR_MIGRATION_PROCESS = 27;
    const ERR_MIGRATION_FINISH = 28;
    const ERR_MAX_DATAVERSION = 29;
    const ERR_VALIDATION = 40;
    const ERR_NOT_FOUND = 404;
    const ERR_HAS_BEEN_DELETED = 61;
    const ERR_INVALID_METHOD = 66;
    const ERR_DEVICE_BLOCK = 67;
    const ERR_PROCESS = 88;
    const ERR_INVALID_KEY = 90;
    const ERR_SYSTEM = 99;

    /**
     * Status Code with Message for api response
     * @var Array
     */
    private $_messages = [
        200 => 'Berhasil',
        204 => 'Berhasil',
        500 => 'Internal Server Error',
        403 => 'Forbidden',
        400 => 'Invalid Argument',
        4 => 'Parameter tidak valid',
        401 => 'Unauthorized',
        6 => 'Akun tidak aktif',
        7 => 'Invalid session',
        8 => 'Akses tidak dibolehkan atau kamu tidak memiliki akses',
        9 => 'Akses tidak valid karena token tidak cocok atau login sudah kadaluarsa',
        10 => 'Akses tidak valid karena akun kamu digunakan untuk login di device lain.',
        11 => 'User tidak valid',
        12 => 'Tanggal transaksi tidak valid',
        13 => 'Outlet tidak valid',
        14 => 'Area tidak valid atau area tidak ditemukan',
        15 => 'Toko belum diaktifkan',
        16 => 'Toko masih dalam proses pembuatan',
        17 => 'Toko kamu diblokir, silahkan hubungi Qasir melalui email hello@qasir.id',
        430 => 'Data transaksi tidak valid atau tidak sesuai',
        431 => 'Transaksi Duplikat',
        23 => 'Data tidak ditemukan',
        24 => 'User ini telah dihapus, mohon hubungi atasan kamu.',
        25 => 'Kamu tidak terdaftar di outlet ini',
        26 => 'User ini telah dihapus, mohon hubungi atasan kamu.',
        27 => 'Toko Anda Sedang dalam Perbaikan, Mohon ditunggu.',
        28 => 'Proses perbaikan Qasir Sudah Selesai, Silahkan Masuk Kembali.',
        29 => 'Data baru terlalu besar, silahkan lakukan pembaharuan terlebih dahulu.',
        40 => 'Validasi tidak cocok',
        404 => 'Data tidak ditemukan',
        61 => 'Has been deleted',
        66 => 'Bad Requests',
        67 => 'Device Kamu sudah login lebih dari 5 merchant, Silahkan Hubungi Tim Qasir.',
        88 => 'Proses gagal',
        90 => 'API key tidak valid',
        99 => 'Internal system error',
    ];

    private $key;
    public $status = self::SUCCESS;
    public $message = 'Success';
    public $data = [];
    public $token;

    /**
     * Set status variabel
     * @param Integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        if (!empty($this->_messages[$status])) {
            $this->message = $this->_messages[$status];
            if ($this->status != self::SUCCESS) {
                if (!isset($this->data['errors'])) {
                    $this->setMessage($this->message);
                }
            }
        }
    }

    /**
     * Get Status Value
     * @return Integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status message
     * @param String $msg
     */
    public function setMessage($msg)
    {
        $this->setData([$msg], 'errors');
    }

    /**
     * Get status message only
     * @return String
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set value of the token
     * @param String $token
     */
    public function setToken($value)
    {
        $this->token = $value;
    }

    /**
     * Get value of the token
     * @return String
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set Data return for api response
     * @param All $data
     * @param String $key
     */
    public function setData(
        $data,
        $key = ''
    )
    {
        if (empty($key)) {
            $this->key = '';
            $this->data = array_merge($this->data, $data);
        } else {
            $this->key = $key;
            $this->data[$key] = $data;
        }
    }

    /**
     * Get all data
     * @return Array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * this function for remove data
     *
     * @param string $value The value
     * @return Array
     */
    public function removeData($value)
    {
        if (!empty($this->data[$value])) {
            unset($this->data[$value]);
            $this->setData($this->data);
            return $this->data;
        }
    }

    /**
     * get status and message
     * @return Array
     */
    public function getStatusMessage()
    {
        return [
            'status' => $this->status,
            'message' => $this->message,
        ];
    }

    /**
     * return all value in this class for api response purpose
     * @return Array
     */
    public function toArray()
    {
        $return = array(
            'status' => $this->status,
            'message' => $this->message,
        );

        if (!empty($this->token)) {
            $return['token'] = $this->token;
        } else {
//            $return['token'] = null;
        }

        if (!empty($this->data)) {
            $return['data'] = $this->data;
        } else {
//            $return['data'] = null;
        }

        return $return;
    }
}
