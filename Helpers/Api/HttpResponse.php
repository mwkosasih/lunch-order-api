<?php namespace Helpers\Api;

class HttpResponse
{
    public static $HTTP_SUCCESS              = 200;
    public static $HTTP_SUCCESS_NO_CONTENT   = 204;
    public static $HTTP_ERROR                = 400;
    public static $HTTP_FORBIDDEN            = 403;
    public static $HTTP_UNAUTHORIZED         = 401;
    public static $HTTP_NOT_FOUND            = 404;
    public static $HTTP_DUPLICATETRANSACTION = 431;
    public static $HTTP_INVALID_TRANSACTION  = 430;
    public static $HTTP_INTERNAL_ERROR       = 500;
}
