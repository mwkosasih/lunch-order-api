<?php namespace Helpers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class Paginate
{
    /**
     * generate for pagination only without data
     *
     * @param  $paginate Collection
     * @param  $page     int
     * @return array
     */
    public static function generate(
        $paginate,
        $page = 1,
        $page_size = 15
    ) {
        return [
            "pageSize"     => $page_size,
            "currentPage"  => (int) $page ?: 1,
            "totalPages"   => $paginate->lastPage(),
            "totalResults" => $paginate->total(),
        ];
    }

    public static function paginate(
        $collection,
        $page = 1,
        $page_size = 15
    ) {
        $total_collection = $collection->count();
        return [
            "pageSize"     => (int) $page_size,
            "currentPage"  => (int) $page ?: 1,
            "totalPages"   => ceil($total_collection / $page_size),
            "totalResults" => $total_collection,
        ];
    }

    /**
     * this function for generate pagination data
     *
     * @param  array   $data_array_paginate The data array paginate
     * @param  integer $page                The page
     * @param  string  $object_key          The object key
     * @return array
     */
    public static function pagination(
        $data_array_paginate = [],
        $page = 1,
        $object_key = 'data',
        $page_size = 15
    ) {
        // set return data
        $pagination = [
            "pageSize"     => $page_size,
            "currentPage"  => (int) $page ?: 1,
            "totalPages"   => $data_array_paginate->lastPage(),
            "totalResults" => $data_array_paginate->total(),
        ];

        // set paginate
        $data_array_paginate = $data_array_paginate->transform(function ($item) {
            return $item;
        })->values()->all();

        $param = [
            'pagination' => $pagination,
            $object_key  => $data_array_paginate,
        ];

        return $param;
    }

    /**
     * Generate pagination based on collection or array
     *
     * @param  array|Collection       $items
     * @param  int                    $perPage
     * @param  int                    $page
     * @param  array                  $options
     * @return LengthAwarePaginator
     */
    public static function collection(
        $items,
        $perPage = 15,
        $page = null,
        $options = []
    ) {
        $page            = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items           = $items instanceof Collection ? $items : Collection::make($items);
        $options['path'] = Paginator::resolveCurrentPath();
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
