<?php namespace Mapping;

use App\Models\Item;
use Mapping\MappingReturn;

class ItemMapping extends MappingReturn
{

    public function mappingListItems($items)
    {
        $resp_items = [];
        foreach ($items as $k => $v) {
            $resp_items[$k]['id'] = $v->id;
            $resp_items[$k]['name'] = $v->name;
            $resp_items[$k]['price'] = $v->price;
            $resp_items[$k]['status'] = $this->mappingStatusItem($v->status);
            $resp_items[$k]['status_type'] = $v->status;
        }
        return $resp_items;
    }

    public function mappingDetailItem($item)
    {
        return [
            'id' => $item->id,
            'name' => $item->name,
            'price' => $item->price,
            'status' => $this->mappingStatusItem($item->status),
            'status_type' => $item->status,
        ];
    }

    private function mappingStatusItem($status)
    {
        $resp = '';
        if ($status == Item::STATUS_ACTIVE) {
            $resp = 'active';
        }
        if ($status == Item::STATUS_INACTIVE) {
            $resp = 'inactive';
        }
        return $resp;
    }

    private function mappingAccessUser($access)
    {
        $resp = '';
        if ($access == User::ACCESS_ADMIN) {
            $resp = 'admin';
        }
        if ($access == User::ACCESS_USER) {
            $resp = 'user';
        }
        if ($access == User::ACCESS_STAFF) {
            $resp = 'staff';
        }
        return $resp;
    }
}
