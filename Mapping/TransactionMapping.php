<?php namespace Mapping;

use App\Models\Transaction;
use App\Models\TransactionItem;
use Mapping\MappingReturn;

class TransactionMapping extends MappingReturn
{

    public function mappingListTransaction($trxs)
    {
        $resp_transactions = [];
        foreach ($trxs as $k => $v) {
            // count total bill
            $trxItems = $v->transactionItems;
            $total_bill = 0;
            for ($x = 0; $x < count($trxItems); $x++) {
                $total_bill += $trxItems[$x]['price'] * $trxItems[$x]['quantity'];
            }

            $resp_transactions[$k]['id'] = $v->id;
            $resp_transactions[$k]['user_id'] = $v->user_id;
            $resp_transactions[$k]['user_name'] = $v->users->name;
            $resp_transactions[$k]['created_at'] = $v->created_at;
            $resp_transactions[$k]['total'] = $total_bill;
            $resp_transactions[$k]['status'] = $v->status;
            $resp_transactions[$k]['status_message'] = $this->mappingStatusTransaction($v->status);
        }
        return $resp_transactions;
    }

    public function mappingDetailTransaction($trx)
    {
        $resp_transactions = [];

        $trxItems = $trx->transactionItems;
        $total_bill = 0;
        $resp_trx_items = [];
        for ($x = 0; $x < count($trxItems); $x++) {
            $total_bill += $trxItems[$x]['price'] * $trxItems[$x]['quantity'];
            $resp_trx_items[$x]['id'] = $trxItems[$x]['id'];
            $resp_trx_items[$x]['transaction_id'] = $trxItems[$x]['transaction_id'];
            $resp_trx_items[$x]['item_id'] = $trxItems[$x]['item_id'];
            $resp_trx_items[$x]['item_name'] = $trxItems[$x]['item_name'];
            $resp_trx_items[$x]['is_additional'] = $trxItems[$x]['is_additional'];
            $resp_trx_items[$x]['price'] = $trxItems[$x]['price'];
            $resp_trx_items[$x]['quantity'] = $trxItems[$x]['quantity'];
            $resp_trx_items[$x]['note'] = $trxItems[$x]['note'];
            $resp_trx_items[$x]['created_at'] = $trxItems[$x]['created_at'];
        }

        $resp_transactions['id'] = $trx->id;
        $resp_transactions['user_id'] = $trx->user_id;
        $resp_transactions['user_name'] = $trx->users->name;
        $resp_transactions['created_at'] = $trx->created_at;
        $resp_transactions['total'] = $total_bill;
        $resp_transactions['status'] = $trx->status;
        $resp_transactions['status_message'] = $this->mappingStatusTransaction($trx->status);
        $resp_transactions['items'] = $resp_trx_items;

        return $resp_transactions;
    }

    private function mappingStatusTransaction($status)
    {
        $resp = '';
        if ($status == Transaction::STATUS_FINISH) {
            $resp = 'Selesai';
        }
        if ($status == Transaction::STATUS_WAITING) {
            $resp = 'Sedang diproses';
        }
        if ($status == Transaction::STATUS_CANCEL) {
            $resp = 'Gagal';
        }
        return $resp;
    }


}
