<?php namespace Mapping;

use App\Models\User;
use App\Models\UserBalances;
use Mapping\MappingReturn;

class UserMapping extends MappingReturn
{

    public function mappingUsersBalances($users)
    {
        $resp_users = [];
        foreach ($users as $k => $user) {
            $resp_users[$k]['id'] = $user->id;
            $resp_users[$k]['name'] = $user->name;
            $resp_users[$k]['balance'] = $user->balance;
        }
        return $resp_users;
    }

    public function mappingDetailUserBalances($userBalances)
    {
        $resp_balance = [];
        foreach ($userBalances as $k => $v) {
            $resp_balance[$k]['id'] = $v->id;
            $resp_balance[$k]['total'] = $v->total;
            $resp_balance[$k]['created_at'] = $v->created_at;
            $resp_balance[$k]['type'] = $v->type;
            $resp_balance[$k]['type_message'] = $this->mappingTypeBalances($v->type);
        }

        return $resp_balance;
    }

    private function mappingTypeBalances($type)
    {
        $resp = '';
        if ($type == UserBalances::TYPE_IN) {
            $resp = 'Saldo Masuk';
        }
        if ($type == UserBalances::TYPE_REFUND) {
            $resp = 'Saldo Refund';
        }
        if ($type == UserBalances::TYPE_PURCHASE) {
            $resp = 'Saldo Keluar';
        }
        return $resp;
    }

    public function mappingListUsers($users)
    {
        $resp_users = [];
        foreach ($users as $k => $user) {
            $resp_users[$k]['id'] = $user->id;
            $resp_users[$k]['name'] = $user->name;
            $resp_users[$k]['email'] = $user->email;
            $resp_users[$k]['balance'] = $user->balance;
            $resp_users[$k]['access'] = $this->mappingAccessUser($user->access);
            $resp_users[$k]['access_type'] = $user->access;
            $resp_users[$k]['status'] = $this->mappingStatusUser($user->status);
            $resp_users[$k]['status_type'] = $user->status;
        }
        return $resp_users;
    }

    public function mappingDetailUsers($user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'balance' => $user->balance,
            'access' => $this->mappingAccessUser($user->access),
            'access_type' => $user->access,
            'status' => $this->mappingStatusUser($user->status),
            'status_type' => $user->status,
        ];
    }

    private function mappingStatusUser($status)
    {
        $resp = '';
        if ($status == User::STATUS_APPROVE) {
            $resp = 'approve';
        }
        if ($status == User::STATUS_WAITING) {
            $resp = 'waiting approval';
        }
        if ($status == User::STATUS_REJECT) {
            $resp = 'rejected';
        }
        return $resp;
    }

    private function mappingAccessUser($access)
    {
        $resp = '';
        if ($access == User::ACCESS_ADMIN) {
            $resp = 'admin';
        }
        if ($access == User::ACCESS_USER) {
            $resp = 'user';
        }
        if ($access == User::ACCESS_STAFF) {
            $resp = 'staff';
        }
        return $resp;
    }
}
