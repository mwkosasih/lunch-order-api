<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Transaction extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    use SoftDeletes;

    CONST STATUS_FINISH = 1;
    CONST STATUS_WAITING = 2;
    CONST STATUS_CANCEL = 3;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'status'
    ];

    public function transactionItems()
    {
        return $this->hasMany(TransactionItem::class, 'transaction_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
