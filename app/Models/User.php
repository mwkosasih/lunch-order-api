<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    use SoftDeletes;

    CONST STATUS_APPROVE = 1;
    CONST STATUS_WAITING = 2;
    CONST STATUS_REJECT = 3;

    CONST ACCESS_ADMIN = 1;
    CONST ACCESS_USER = 2;
    CONST ACCESS_STAFF = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'balance', 'access', 'status', 'token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function balances()
    {
        return $this->hasMany(UserBalances::class)->withTrashed();
    }
}
