<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class UserBalances extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    CONST TYPE_IN = 1;
    CONST TYPE_REFUND = 2;
    CONST TYPE_PURCHASE = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'total', 'date', 'note', 'type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
}
