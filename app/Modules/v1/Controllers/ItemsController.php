<?php

namespace App\Modules\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Helpers\Api\ApiResponse;
use Helpers\Api\HttpResponse;
use Illuminate\Http\Request;
use Auth;
use Mapping\ItemMapping;
use Helpers\Paginate;

class ItemsController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ApiResponse;
        $this->mappingg = new ItemMapping();
    }

    public function list(Request $request)
    {
        $items = Item::orderBy('id', 'desc');
        // search item based by name
        if (!empty($request->has('name'))) {
            $items = $items->where('name', 'LIKE', '%' . $request->input('name') . '%');
        }
        // search user based by status
        if (!empty($request->has('status'))) {
            $users = $items->where('status', '=', $request->input('status'));
        }

        //get all item for paginate
        $limit = 15;
        if (!empty($request->input('count'))) {
            $limit = $request->input('count');
        }
        $items = $items->paginate($limit);

        // call function pagination
        $pagination = Paginate::generate($items, $request->input('page'));
        // mapping list items
        $items = $this->mappingg->mappingListItems($items);

        $this->response->setData($items, 'items');
        $this->response->setData($pagination, 'pagination');
        $this->response->setToken(Auth::user()->token);
        $this->response->setStatus(ApiResponse::SUCCESS);
        return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
    }

    public function detail(Request $request, $id = '')
    {
        $item = Item::where('id', $id)->first();

        if (empty($item)) {
            $this->response->setMessage('Item tidak ditemukan');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        } else {
            $item = $this->mappingg->mappingDetailItem($item);

            $this->response->setData($item, 'item');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        }
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'status' => 'required',
        ]);

        // request create item
        $data = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'status' => $request->input('status'),
        ];
        if (Item::create($data)) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Gagal membuat item');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);

        }
    }

    public function update(Request $request, $id = '')
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'status' => 'required',
        ]);

        $user = Item::where('id', $id)->first();
        if (!$user) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('Item tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        return $this->updateItem($request, $user);
    }

    private function updateItem($request, $user)
    {
        // request update item
        $data = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'status' => $request->input('status'),
        ];

        if ($user->update($data)) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Update item gagal');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);
        }
    }

    public function delete($id)
    {
        $item = Item::where('id', $id)->first();
        if (!$item) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('Item tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        if ($item->delete()) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Hapus item gagal');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);
        }
    }

    public function updateStatus(Request $request, $id = '')
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $item = Item::where('id', $id)->first();
        if (!$item) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('Item tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        $status = [
            'status' => $request->input('status')
        ];
        if ($item->update($status)) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Update status item gagal');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);
        }
    }
}
