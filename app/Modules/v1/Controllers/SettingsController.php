<?php

namespace App\Modules\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Helpers\Api\ApiResponse;
use Helpers\Api\HttpResponse;
use Illuminate\Http\Request;
use Auth;

class SettingsController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ApiResponse;
    }

    public function detail(Request $request, $key = '')
    {
        $setting = Setting::where('key', $key)->first();

        if (empty($setting)) {
            $this->response->setMessage('Pengaturan tidak ditemukan');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        } else {
            $resp = [
                'value' => $setting->value
            ];
            $this->response->setData($resp);
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        }
    }


    public function update(Request $request, $key = '')
    {
        $this->validate($request, [
            'value' => 'required'
        ]);

        $setting = Setting::where('key', $key)->first();
        if (!$setting) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('Pengaturan tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        // request update setting
        $data = [
            'value' => $request->input('value'),
        ];
        if ($setting->update($data)) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Update pengaturan gagal');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);
        }
    }
}
