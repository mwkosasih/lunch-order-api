<?php

namespace App\Modules\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Transaction;
use App\Models\TransactionItem;
use App\Models\UserBalances;
use Helpers\Api\ApiResponse;
use Helpers\Api\HttpResponse;
use Helpers\Paginate;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Carbon\Carbon;
use Mapping\TransactionMapping;

class TransactionsController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ApiResponse;
        $this->mapping = new TransactionMapping();
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'carts' => 'required',
            'carts.*.is_additional' => 'required|numeric',
            'carts.*.item_name' => 'required',
            'carts.*.price' => 'required',
            'carts.*.quantity' => 'required',
        ]);

        $total_bill = 0;
        $cart = $request->input('carts');
        for ($x = 0; $x < count($cart); $x++) {
            // validate item
            if ($cart[$x]['item_id'] != 0) {
                $item = Item::where('id', $cart[$x]['item_id'])->where('status', Item::STATUS_ACTIVE)->first();
                if (empty($item)) {
                    $this->response->setMessage('Item tidak ditemukan');
                    $this->response->setToken(Auth::user()->token);
                    $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
                    return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
                }
            }
            $total_bill += $cart[$x]['price'] * $cart[$x]['quantity'];
        }

        // check user saldo
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->balance < $total_bill) {
            $this->response->setMessage('Saldo tidak mencukupi untuk melakukan transaksi');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_INVALID_TRANSACTION);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_ERROR);
        }

        $this->updateUserBalances($user, $total_bill, UserBalances::TYPE_PURCHASE);
        return $this->createTransaction($request, $cart);
    }

    public function updateUserBalances($user, $total_bill, $type)
    {
        $note = "";
        $user_balance = [];
        if ($type == UserBalances::TYPE_PURCHASE) {
            $user_balance = [
                'balance' => $user->balance - $total_bill
            ];
            $note = "TRANSACTION_PURCHASE";
        }
        if ($type == UserBalances::TYPE_REFUND) {
            $user_balance = [
                'balance' => $user->balance + $total_bill
            ];
            $note = "TRANSACTION_REFUND";
        }

        $user->update($user_balance);

        $data = [
            'user_id' => $user->id,
            'total' => $total_bill,
            'date' => Carbon::now(),
            'note' => $note,
            'type' => $type
        ];
        UserBalances::create($data);
    }

    public function createTransaction($request, $cart)
    {
        // create transaction
        $data_transaction = [
            'user_id' => Auth::user()->id,
            'status' => Transaction::STATUS_WAITING
        ];
        $result = Transaction::create($data_transaction);
        $trx_id = $result->id;
        // create transaction items
        foreach ($cart as $v) {
            $data_transaction_items = [
                'transaction_id' => $trx_id,
                'item_id' => $v['item_id'],
                'item_name' => $v['item_name'],
                'is_additional' => $v['is_additional'],
                'price' => $v['price'],
                'quantity' => $v['quantity'],
                'note' => $v['note'],
            ];
            TransactionItem::create($data_transaction_items);
        }

        $this->response->setStatus(ApiResponse::SUCCESS);
        return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
    }

    public function list(Request $request)
    {
        $trxs = Transaction::orderBy('id', 'desc');
        // search transaction based by user id
        if (!empty($request->has('user_id'))) {
            $trxs = $trxs->where('user_id', '=', $request->input('user_id'));
        }
        // search transaction based by created at
        if (!empty($request->has('date'))) {
            $trxs = $trxs->where('created_at', 'LIKE', '%' . $request->input('date') . '%');
        }
        //get all item for paginate
        $limit = 15;
        if (!empty($request->input('count'))) {
            $limit = $request->input('count');
        }
        $trxs = $trxs->paginate($limit);

        // call function pagination
        $pagination = Paginate::generate($trxs, $request->input('page'));
        // mapping list transaction
        $trxs = $this->mapping->mappingListTransaction($trxs);

        $this->response->setData($trxs, 'transactions');
        $this->response->setData($pagination, 'pagination');
        $this->response->setToken(Auth::user()->token);
        $this->response->setStatus(ApiResponse::SUCCESS);
        return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
    }

    public function detail(Request $request, $id = '')
    {
        $trx = Transaction::where('id', $id)->first();

        if (empty($trx)) {
            $this->response->setMessage('Transaksi tidak ditemukan');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        } else {
            $trx = $this->mapping->mappingDetailTransaction($trx);

            $this->response->setData($trx, 'transaction');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        }
    }

    public function update(Request $request, $id = '')
    {
        $this->validate($request, [
            'status' => 'required',
            'carts' => 'required',
            'carts.*.transaction_item_id' => 'required|numeric',
            'carts.*.is_additional' => 'required|numeric',
            'carts.*.item_name' => 'required',
            'carts.*.price' => 'required',
            'carts.*.quantity' => 'required',
        ]);

        // find transaction based by request id
        $trx = Transaction::where('id', $id)->first();
        if (empty($trx)) {
            $this->response->setMessage('Transaksi tidak ditemukan');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }
        // find user
        $user = User::where('id', $trx->user_id)->first();
        // count total bill before
        $trxItems = $trx->transactionItems;
        $total_bill_before = 0;
        for ($x = 0; $x < count($trxItems); $x++) {
            $total_bill_before += $trxItems[$x]['price'] * $trxItems[$x]['quantity'];
        }
        // count total bill in cart
        $total_bill = 0;
        $cart = $request->input('carts');
        for ($x = 0; $x < count($cart); $x++) {
            $total_bill += $cart[$x]['price'] * $cart[$x]['quantity'];
        }

        // update transaction & refund user balance if request status is failed / cancel
        if ($request->input('status') == Transaction::STATUS_CANCEL) {
            // update trx
            $this->updateTransaction($request, $trx);
            // refund user balance
            $this->updateUserBalances($user, $total_bill, UserBalances::TYPE_REFUND);
        }
        // update transaction & refund user balance if request status is finish
        if ($request->input('status') == Transaction::STATUS_FINISH) {
            // update trx
            $this->updateTransaction($request, $trx);
            // refund user balance
            if ($total_bill > $total_bill_before) {
                $this->updateUserBalances($user, $total_bill, UserBalances::TYPE_PURCHASE);
            }
            // purchase user balance
            if ($total_bill < $total_bill_before) {
                $this->updateUserBalances($user, $total_bill, UserBalances::TYPE_REFUND);
            }
        }

        $this->response->setStatus(ApiResponse::SUCCESS);
        return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
    }

    public function updateTransaction($request, $transaction)
    {
        // update transaction
        $data_transaction = [
            'status' => $request->input('status'),
        ];
        $transaction->update($data_transaction);
        // update transaction items
        foreach ($request->input('carts') as $v) {
            // find transaction items based by request transaction_item_id
            $trxItem = TransactionItem::where('id', $v['transaction_item_id'])->first();
            if (empty($trxItem)) {
                $this->response->setMessage('Transaksi Items tidak ditemukan');
                $this->response->setToken(Auth::user()->token);
                $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
                return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
            }
            // update transaction items
            $data_transaction_items = [
                'transaction_id' => $transaction->id,
                'item_id' => $v['item_id'],
                'item_name' => $v['item_name'],
                'is_additional' => $v['is_additional'],
                'price' => $v['price'],
                'quantity' => $v['quantity'],
                'note' => $v['note'],
            ];
            $trxItem->update($data_transaction_items);
        }
    }
}
