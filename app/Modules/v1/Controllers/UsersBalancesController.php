<?php

namespace App\Modules\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Models\UserBalances;
use Helpers\Api\ApiResponse;
use Helpers\Api\HttpResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Mapping\UserMapping;
use Helpers\Paginate;
use Carbon\Carbon;

class UsersBalancesController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ApiResponse;
        $this->mapping = new UserMapping();
    }

    public function list(Request $request)
    {
        $users = User::where('status', User::STATUS_APPROVE)->orderBy('id', 'desc');
        // search user based by name
        if (!empty($request->has('name'))) {
            $users = $users->where('name', 'LIKE', '%' . $request->input('name') . '%');
        }

        //get all user for paginate
        $limit = 15;
        if (!empty($request->input('count'))) {
            $limit = $request->input('count');
        }
        $users = $users->paginate($limit);

        // call function pagination
        $pagination = Paginate::generate($users, $request->input('page'));
        if (count($users) == 0) {
            $this->response->setMessage('List Saldo User tidak ditemukan');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        } else {
            $users = $this->mapping->mappingUsersBalances($users);

            $this->response->setData($users, 'users_balances');
            $this->response->setData($pagination, 'pagination');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        }
    }

    public function detail(Request $request)
    {
        $user = User::where('id', $request->input('user_id'))
            ->where('status', User::STATUS_APPROVE)->first();
        if (empty($user)) {
            $this->response->setMessage('User tidak ditemukan');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        $userBalances = UserBalances::where('user_id', $request->input('user_id'));
        // search user balance based by created_at
        if (!empty($request->has('date'))) {
            $userBalances = $userBalances->where('created_at', 'LIKE', '%' . $request->input('date') . '%');
        }

        //get all item for paginate
        $limit = 15;
        if (!empty($request->input('count'))) {
            $limit = $request->input('count');
        }
        $userBalances = $userBalances->paginate($limit);

        // call function pagination
        $pagination = Paginate::generate($userBalances, $request->input('page'));
        // mapping user data & user balance data
        $response = $this->mapping->mappingDetailUserBalances($userBalances);
        $responseUserData = $this->mapping->mappingDetailUsers($user);

        $this->response->setData($responseUserData, 'user');
        $this->response->setData($response, 'user_balances');
        $this->response->setData($pagination, 'pagination');
        $this->response->setToken(Auth::user()->token);
        $this->response->setStatus(ApiResponse::SUCCESS);
        return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|numeric',
            'balances' => 'required|numeric'
        ]);

        // find user by email
        $user = User::where('id', $request->input('user_id'))
            ->where('status', User::STATUS_APPROVE)
            ->first();
        if (empty($user)) {
            $this->response->setMessage('User tidak ditemukan');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        // update user balances in user table
        $data = [
            'balance' => $user->balance + $request->input('balances')
        ];
        $user->update($data);

        return $this->createUserBalances($request, $user);
    }

    private function createUserBalances($request, $user)
    {
        // request create user balances in user balances table
        $data = [
            'user_id' => $user->id,
            'total' => $request->input('balances'),
            'date' => Carbon::now(),
            'note' => $request->input('note'),
            'type' => UserBalances::TYPE_IN
        ];
        if (UserBalances::create($data)) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Gagal menyimpan saldo user');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);

        }
    }
}
