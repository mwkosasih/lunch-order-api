<?php

namespace App\Modules\v1\Controllers;

use App\Http\Controllers\Controller;
use Helpers\Api\ApiResponse;
use Helpers\Api\HttpResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Mapping\UserMapping;
use Helpers\Paginate;
use Carbon\Carbon;

class UsersController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ApiResponse;
        $this->mapping = new UserMapping();
    }

    public function list(Request $request)
    {
        $users = User::orderBy('id', 'desc');
        // search user based by name
        if (!empty($request->has('name'))) {
            $users = $users->where('name', 'LIKE', '%' . $request->input('name') . '%');
        }
        // search user based by status
        if (!empty($request->has('status'))) {
            $users = $users->where('status', '=', $request->input('status'));
        }

        //get all user for paginate
        $limit = 15;
        if (!empty($request->input('count'))) {
            $limit = $request->input('count');
        }
        $users = $users->paginate($limit);

        // call function pagination
        $pagination = Paginate::generate($users, $request->input('page'));
        // mapping list users
        $users = $this->mapping->mappingListUsers($users);

        $this->response->setData($users, 'users');
        $this->response->setData($pagination, 'pagination');
        $this->response->setToken(Auth::user()->token);
        $this->response->setStatus(ApiResponse::SUCCESS);
        return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
    }

    public function detail(Request $request, $id = '')
    {
        $user = User::where('id', $id)->first();

        if (empty($user)) {
            $this->response->setMessage('User tidak ditemukan');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        } else {
            $user = $this->mapping->mappingDetailUsers($user);

            $this->response->setData($user, 'user');
            $this->response->setToken(Auth::user()->token);
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        }
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'status' => 'required',
            'type' => 'required'
        ]);

        // find user by email
        $findByEmail = User::where('email', $request->input('email'))->first();
        if (isset($findByEmail)) {
            $this->response->setStatus(ApiResponse::ERR_DUPLICATE_TRANSACTION);
            $this->response->setMessage('Email sudah digunakan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_DUPLICATETRANSACTION);
        }

        return $this->createUser($request);
    }

    private function createUser($request)
    {
        // request create user
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'access' => $request->input('type'),
            'status' => $request->input('status'),
            'balance' => 0,
            'token' => base64_encode(str_random(40))
        ];
        if (User::create($data)) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Gagal membuat user');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);

        }
    }

    public function update(Request $request, $id = '')
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'status' => 'required',
            'type' => 'required'
        ]);

        $user = User::where('id', $id)->first();
        if (!$user) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('User tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        return $this->updateUser($request, $user);
    }

    private function updateUser($request, $user)
    {
        // request update user
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'access' => $request->input('type'),
            'status' => $request->input('status')
        ];

        if ($user->update($data)) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Update user gagal');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);
        }
    }

    public function delete($id)
    {
        $user = User::where('id', $id)->first();
        if (!$user) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('User tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        if ($user->delete()) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Hapus user gagal');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);
        }
    }

    public function updateStatus(Request $request, $id = '')
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $user = User::where('id', $id)->first();
        if (!$user) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('User tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        $status = [
            'status' => $request->input('status')
        ];
        if ($user->update($status)) {
            $this->response->setStatus(ApiResponse::SUCCESS);
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
        } else {
            $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
            $this->response->setMessage('Update status user gagal');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);
        }
    }

    public function updatePassword(Request $request, $id = '')
    {
        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('id', Auth::user()->id)->first();
        if (!$user) {
            $this->response->setStatus(ApiResponse::ERR_NOT_FOUND);
            $this->response->setMessage('User tidak ditemukan');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_NOT_FOUND);
        }

        $current_password = $request->input('current_password');
        if (Hash::check($current_password, $user->password)) {
            $data = [
                'password' => Hash::make($request->input('password'))
            ];
            if ($user->update($data)) {
                $this->response->setStatus(ApiResponse::SUCCESS);
                return response()->json($this->response->toArray(), HttpResponse::$HTTP_SUCCESS);
            } else {
                $this->response->setStatus(ApiResponse::ERR_INTERNAL_ERROR);
                $this->response->setMessage('Update password user gagal');
                return response()->json($this->response->toArray(), HttpResponse::$HTTP_INTERNAL_ERROR);
            }
        } else {
            $this->response->setStatus(ApiResponse::ERR_INVALID_LOGIN);
            $this->response->setMessage('Password lama tidak sesuai');
            return response()->json($this->response->toArray(), HttpResponse::$HTTP_FORBIDDEN);
        }
    }
}
