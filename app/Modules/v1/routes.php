<?php

$router->group(
    [
        'prefix' => 'v1',
    ],
    function () use ($router) {
        $router->get('/', function () use ($router) {
            return "Hello World!";
        });
        $router->post('/register', 'AuthController@register');
        $router->post('/login', 'AuthController@login');

        $router->group(
            [
                'middleware' => 'auth',
            ],
            function () use ($router) {
                // users
                $router->get('users/', 'UsersController@list');
                $router->get('user/{id}', 'UsersController@detail');
                $router->post('user/', 'UsersController@create');
                $router->put('user/{id}', 'UsersController@update');
                $router->put('user/{id}/status', 'UsersController@updateStatus');
                $router->put('user/change/password', 'UsersController@updatePassword');
                $router->delete('user/{id}', 'UsersController@delete');

                // users balances
                $router->get('users/balances', 'UsersBalancesController@list');
                $router->get('user/balances/detail', 'UsersBalancesController@detail');
                $router->post('user/balances', 'UsersBalancesController@create');

                // items
                $router->get('items', 'ItemsController@list');
                $router->get('item/{id}', 'ItemsController@detail');
                $router->post('item/', 'ItemsController@create');
                $router->put('item/{id}', 'ItemsController@update');
                $router->put('item/{id}/status', 'ItemsController@updateStatus');
                $router->delete('item/{id}', 'ItemsController@delete');

                // transactions
                $router->post('transaction', 'TransactionsController@create');
                $router->get('transactions', 'TransactionsController@list');
                $router->get('transaction/{id}', 'TransactionsController@detail');
                $router->put('transaction/{id}', 'TransactionsController@update');

                // settings
                $router->get('setting/{key}', 'SettingsController@detail');
                $router->put('setting/{key}', 'SettingsController@update');
            });
    });
