<?php namespace App\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request as FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Helpers\Api\ApiResponse;
use Helpers\Api\HttpResponse;

abstract class Request extends FormRequest
{
    protected $response;

    /**
     * this function for api response
     */
    public function __construct()
    {
        $this->response = new ApiResponse;
    }

    /**
     * this function for declare response
     *
     * @param  array          $errors The errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        $data   = [];
        $errors = array_values($errors);
        foreach ($errors as $error) {
            foreach ($error as $value) {
                $data[] = $value;
            }
        }
        $data = array_values(array_unique($data));
        // return data error
        $this->response->setData($data, 'errors');
        $this->response->setStatus(ApiResponse::ERR_INVALID_PARAMETER);
        return new JsonResponse($this->response->toArray(), 400);
    }

    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator, $this->response(
            $this->formatErrors($validator)
        ));
    }

    /**
     * Overide function for all parameters to add routes parameters.
     * @return array
     */
    public function all()
    {
        return array_replace_recursive(parent::all(), $this->route()->parameters());
    }
}
