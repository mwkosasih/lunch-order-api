<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->default('');
            $table->string('email', 60)->default('');
            $table->string('password', 255)->default('');
            $table->double('balance')->nullable();
            $table->tinyInteger('access')->comment('1: admin, 2: user, 3: staff');
            $table->tinyInteger('status')->comment('1: approve, 2: waiting, 3: reject');
            $table->string('token', 255)->default('');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id', 'users_migrations_id_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
