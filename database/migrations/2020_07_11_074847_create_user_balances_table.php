<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->double('total');
            $table->dateTime('date');
            $table->string('note', 255)->nullable();
            $table->tinyInteger('type')->comment('1: in, 2: refund, 3: purchase');
            $table->timestamps();

            $table->index('id', 'user_balances_migrations_id_idx');
            $table->index('user_id', 'user_balances_migrations_user_id_fk_idx');

            $table->foreign('user_id', 'user_balances_migrations_user_id_fk')->references('id')->on('users')->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_balances');
    }
}
