<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaction_id');
            $table->unsignedInteger('item_id')->nullable();
            $table->tinyInteger('is_additional')->comment('0 : false, 1 : true');
            $table->double('price');
            $table->integer('quantity');
            $table->string('note', 255)->nullable();
            $table->timestamps();

            $table->index('id', 'trx_items_migrations_id_idx');
            $table->index('transaction_id', 'trx_items_migrations_transaction_id_fk_idx');

            $table->foreign('transaction_id', 'trx_items_migrations_transaction_id_fk')->references('id')->on('transactions')->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_items');
    }
}
