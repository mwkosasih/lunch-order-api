<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();

        DB::table('settings')->insert([
            ['key' => 'last_order', 'value' => '11.00', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}
